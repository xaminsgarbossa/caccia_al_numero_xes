package com.xes.cacciaalnumero;

/**
 * Created by studente on 12/11/16.
 */

class MyShakeInterpolator implements android.view.animation.Interpolator {
    double mAmplitude = 2;
    double mFrequency = 10;

    MyShakeInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}