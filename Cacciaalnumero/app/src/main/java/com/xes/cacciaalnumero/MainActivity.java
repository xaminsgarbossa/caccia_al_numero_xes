package com.xes.cacciaalnumero;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Debug;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MainActivity extends AppCompatActivity {


    List<Integer> currentSum = new ArrayList<Integer>();
    List<TextView> selectedViews = new ArrayList<TextView>();
    List<Integer> numbers = new ArrayList<Integer>();

    int goal = 10;
    int punti = 0;
    int maxSums = 0;

    public static MainActivity instance;

    void Init(){
        GridView gridView = (GridView) findViewById(R.id.gridview);
        TextView goaltextView = (TextView) findViewById(R.id.targetNumber);
        TextView sumLabel = (TextView) findViewById(R.id.labelSomma);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        limit2 = prefs.getBoolean("2nums",false);
        successString = prefs.getString("rightSum","");
        failureString = prefs.getString("wrongSum","");

        sumLabel.setTextColor(Color.parseColor("#FF9100"));
        sumLabel.setText("TOCCA I NUMERI");

        /// show the dialog here
        goaltextView.setText("- -");
        DialogFragment npd = new NumberPickerDialog();
        npd.setCancelable(false);
        npd.show(this.getFragmentManager(), "nulla");
        ///
    }

    public void FinalInit(int selGoal){
        GridView gridView = (GridView) findViewById(R.id.gridview);
        TextView goaltextView = (TextView) findViewById(R.id.targetNumber);
        TextView sumLabel = (TextView) findViewById(R.id.labelSomma);
        TextView scoreLabel =(TextView) findViewById(R.id.scoreLabel);
        punti = 0;
        goal = selGoal;
        numbers.clear();
        for (int i = 0; i<5*6;i++)
            numbers.add(ThreadLocalRandom.current().nextInt(1, (goal > 9) ? 9 : goal));

        maxSums = CalculateMax(new ArrayList<Integer>(numbers),goal);
        scoreLabel.setText(String.valueOf(punti)+ ((limit2)? "/" + String.valueOf(maxSums):""));
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,
                R.layout.cell, numbers);

        gridView.invalidateViews();
        gridView.setAdapter(adapter);
        for (int i = 0; i<gridView.getChildCount();i++) {
            TextView tv = (TextView) gridView.getChildAt(i);
            Debug.waitForDebugger();
            if (tv != null) {
                tv.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                tv.setEnabled(true);
            }
        }
        goaltextView.setText(String.valueOf(goal));
    }

    int CalculateMax(List<Integer> nums, int targetSum){
        int total = 0;
        Collections.sort(nums);
        Collections.reverse(nums);
        while (nums.size()>0){
            List<Integer> used = new ArrayList<Integer>();
            int localSum = nums.get(0);
            used.add(localSum);
            int i = nums.size()-1;
            while (localSum < targetSum && i>0){
                if (localSum+nums.get(i) == targetSum)
                {
                    localSum += nums.get(i);
                    used.add(nums.get(i));
                }else{
                    if (nums.get(0) + nums.get(nums.size()-1) == targetSum) {
                        total++;
                        nums.remove(0);
                        nums.remove(nums.size()-1);
                        used.clear();
                        break;
                    }
                }
                i--;
            }
            if (targetSum == localSum) {
                RemoveAllOnce(nums,used);
                used.clear();
                total++;
            }
            else {
                nums.remove(0);
            }
        }
        return total;
    }

    void RemoveAllOnce(List<Integer> base, List<Integer> remover){
        for (int i = 0; i<remover.size(); i++){
            int seek = remover.get(i);
            for (int j= 0; j<base.size();j++){
                if (base.get(j) == seek) {
                    base.remove(j);
                    break;
                }
            }
        }
    }

    void  Reset (View v){
        currentSum.clear();
        for (TextView txtView : selectedViews) {
            AnimateColor(txtView, "backgroundColor", Color.TRANSPARENT);
        }
        selectedViews.clear();
        Init();
    }

    boolean lastWasError = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id==R.id.action_settings){
            Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
            startActivity(intent);
            return false;
        }
        if (id==R.id.action_info){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Informazioni");
            builder.setPositiveButton("Fantastico", null);
            builder.setMessage("App sviluppata da\nAlberto Xamin e Enrico Sgarbossa,\nstudenti della classe 5AI\ndell'ITT E.BARSANTI\nnell'anno 2016/2017!");
            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        }
        return super.onOptionsItemSelected(item);
    }

    String successString="";
    String failureString="";
    boolean limit2 = false;

    public enum PageSource{
        Main, Settings
    }

    public static PageSource lastActivity = PageSource.Main;

    @Override
    protected void onResume() {
        if (lastActivity != PageSource.Main){
            lastActivity = PageSource.Main;
            Init();
        }
        super.onResume();
    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_main);
        MenuInflater inflater = getMenuInflater();



        ImageButton resetBtn = (ImageButton) findViewById(R.id.resetButton);
        resetBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Reset(v);
            }
        });

        Init();
        final GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                TextView tv = (TextView) v;

                TextView sumLabel = (TextView) findViewById(R.id.labelSomma);
                if (tv.isEnabled()) {
                    final Animation myAnim = AnimationUtils.loadAnimation(getApplication().getBaseContext(), R.anim.bounce);
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    tv.startAnimation(myAnim);
                    if (lastWasError){
                        for (TextView txtView : selectedViews) {
                            ResetTextView(txtView);
                        }
                        selectedViews.clear();
                        lastWasError = false;
                    }
                    tv.setEnabled(false);
                    AnimateColor(tv,"backgroundColor",getResources().getColor(R.color.coloreOperazione));
                    AnimateColor(tv,"textColor",Color.WHITE);
                    currentSum.add(numbers.get(position));
                    sumLabel.setText("");
                    AnimateColor(sumLabel,"textColor",Color.parseColor("#FF9100"));
                    selectedViews.add(tv);

                    TextView scoreLabel =(TextView) findViewById(R.id.scoreLabel);
                    int cSum = CalculateSum(sumLabel);

                    if (cSum == goal) {
                        currentSum.clear();
                        int currentNiceColor = -ThreadLocalRandom.current().nextInt(1000,16777216);
                        AnimateColor(tv,"backgroundColor", currentNiceColor);
                        for (TextView txtView : selectedViews) {
                            AnimateColor(txtView,"backgroundColor", currentNiceColor);
                            AnimateColor(txtView,"textColor",Color.WHITE);
                        }
                        selectedViews.clear();
                        AnimateColor(sumLabel,"textColor",currentNiceColor);
                        if (successString.isEmpty())
                            successString = "Bravo, continua cosí!";
                        Snackbar sb = Snackbar.make(v, successString, Snackbar.LENGTH_LONG);
                        sb.getView().setBackgroundColor(currentNiceColor);
                        sb.show();
                        punti++;
                        scoreLabel.setText(String.valueOf(punti) + ((limit2)? "/" + String.valueOf(maxSums):""));
                    }
                    else if (cSum > goal || (limit2 && currentSum.size() == 2)) {
                        AnimateColor(sumLabel,"textColor", Color.RED);
                        sumLabel.append(" = " + String.valueOf(cSum));
                        if (failureString.isEmpty())
                            failureString = "Controlla il calcolo, questa somma non fa "+String.valueOf(goal)+"!";
                        Snackbar sb = Snackbar.make(v, failureString, Snackbar.LENGTH_LONG);
                        sb.getView().setBackgroundColor(Color.RED);
                        sb.show();
                        currentSum.clear();
                        for (TextView txtView : selectedViews)
                        {
                            AnimateColor(txtView, "backgroundColor", Color.RED);
                            txtView.setEnabled(true);
                        }
                        lastWasError = true;
                    }
                }else {
                    if (selectedViews.contains(tv)) {
                        ResetTextView(tv);
                        selectedViews.remove(tv);
                        currentSum.remove(numbers.get(position));
                        CalculateSum(sumLabel);
                    }else {
                        final Animation myAnim = AnimationUtils.loadAnimation(getApplication().getBaseContext(), R.anim.shake);
                        MyShakeInterpolator interpolator = new MyShakeInterpolator(.4, ThreadLocalRandom.current().nextInt(5,50));
                        myAnim.setInterpolator(interpolator);
                        tv.startAnimation(myAnim);
                    }
                }

            }
        });
    }

    int CalculateSum(TextView sumLabel){
        int cSum = 0;
        sumLabel.setText("");
        for (int n : currentSum) {
            if (sumLabel.getText().length() == 0)
                sumLabel.append(String.valueOf(n));
            else
                sumLabel.append(" + " + n);
            cSum += n;
        }
        if (currentSum.size() == 0)
            sumLabel.setText("TOCCA I NUMERI");
        return cSum;
    }

    void ResetTextView(TextView tv){
        AnimateColor(tv,"backgroundColor",Color.TRANSPARENT);
        AnimateColor(tv,"textColor",Color.BLACK);
        tv.setEnabled(true);
    }

    GradientDrawable GetDrawable(View v){
        return (GradientDrawable) v.getBackground();
    }

    void AnimateColor(View v, String property,int color) {
        if (v != null) {
            if (property == "backgroundColor") {
                v.setBackgroundResource(R.drawable.rounded_corner);
                GetDrawable(v).setColor(color);
            }else {
                final ObjectAnimator animator = ObjectAnimator.ofInt(v, property, color);
                animator.setDuration(333L);
                animator.setEvaluator(new ArgbEvaluator());
                animator.setInterpolator(new DecelerateInterpolator(2));
                animator.start();
            }
        }
    }
}
