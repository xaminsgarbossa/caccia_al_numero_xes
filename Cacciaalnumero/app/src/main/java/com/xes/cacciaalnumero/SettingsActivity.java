package com.xes.cacciaalnumero;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import static android.app.PendingIntent.getActivity;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        ((Switch)findViewById(R.id.twonumbers)).setChecked( prefs.getBoolean("2nums",false));
        ((TextView)findViewById(R.id.rightSum)).setText(    prefs.getString("rightSum",""));
        ((TextView)findViewById(R.id.wrongSum)).setText(    prefs.getString("wrongSum",""));
    }

    void Save(View v){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("2nums", ((Switch)findViewById(R.id.twonumbers)).isChecked());
        editor.putString("rightSum", ((TextView)findViewById(R.id.rightSum)).getText().toString());
        editor.putString("wrongSum", ((TextView)findViewById(R.id.wrongSum)).getText().toString());
        editor.commit();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Le impostazioni sono state salvate");
        builder.setPositiveButton("Torna al gioco", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MainActivity.lastActivity = MainActivity.PageSource.Settings;
                onBackPressed();
            }
        });
        builder.setNegativeButton("Rimani qui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
