package com.xes.cacciaalnumero;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ViewPropertyAnimatorCompatSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alberto on 29-Nov-16.
 */

public class NumberPickerDialog extends DialogFragment {

    View currentView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        currentView =inflater.inflate(R.layout.dialog_reset, null);
        builder.setView(currentView);

        Button shuf = (Button) currentView.findViewById(R.id.shuflleBtn);
        Button cust = (Button) currentView.findViewById(R.id.customBtn);
        shuf.setOnClickListener(shuffle);
        cust.setOnClickListener(custom);
        return builder.create();
    }

    private View.OnClickListener shuffle = new View.OnClickListener() {
        public void onClick(View v) {
            MainActivity.instance.FinalInit(ThreadLocalRandom.current().nextInt(5,21));
            dismiss();
        }
    };

    private View.OnClickListener custom = new View.OnClickListener() {
        public void onClick(View v) {
            EditText number = (EditText)currentView.findViewById(R.id.number);
            int n=Integer.parseInt(number.toString());
            if (!number.getText().toString().isEmpty()) {

                if(n<=20 && n>=5){
                    MainActivity.instance.FinalInit(Integer.parseInt(number.getText().toString()));
                }
                else if(n>20 || n<5){
                    Context context=getActivity();
                    CharSequence txt="Il numero deve essere compreso tra 5 e 20.";
                    int duration=Toast.LENGTH_SHORT;

                    Toast toast=Toast.makeText(context,txt,duration);
                    toast.show();
                }
                dismiss();
            }
        }
    };
}
